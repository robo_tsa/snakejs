var canvas = document.getElementById("canvas");
var ctx = canvas.getContext('2d');
var w = document.getElementById("canvas").getAttribute("width");
//alert(w);
var h = document.getElementById("canvas").getAttribute("height");
//alert(h);

//narysowanie pustego plotna
/*ctx.fillStyle="white";
ctx.fillRect(0,0,w,h);
ctx.strokeStyle="black";
ctx.strokeRect(0,0,w,h);*/

var snakeArray;
var size = 10;
var direction;
var food;
var score = 0;





//rozpoczecie gry
function init(){
    score=0
    direction = "right";
    createSnake();
    createFood()
   
    
    if(typeof game_loop !="undefined") {clearInterval(game_loop)};
    game_loop = setInterval(paint,100);
    

}



createSnake()//funkcja tworzaca weza
function createSnake(){
    
    var length = 5;
    snakeArray=[];
    for(var i=length-1; i>0; i--){
        snakeArray.push({x:i, y:0}); //poprawic petle aby szla od pierwszej gornej komorki
        
    }
    
}


function createFood(){
    
    
    food = {
        x:Math.floor(Math.random() * (w/size)),
        y:Math.floor(Math.random() * (h/size)),
    
    
};}






function paint() {
   
ctx.fillStyle="white";
ctx.fillRect(0,0,w,h);
ctx.strokeStyle="black";
ctx.strokeRect(0,0,w,h);

    
  
    var nx=snakeArray[0].x; //pozycja glowy weza w x
    var ny=snakeArray[0].y; //pozycja glowy weza w y
    //nx++;
    
//funkcja sterowania 
    
    if(direction == "right") nx++;
    else if(direction == "left") nx--;
    else if (direction == "up") ny--;
    else if(direction == "down") ny++;    
    
    
   document.addEventListener("keydown", KeyDown, false);
   function KeyDown(e){
           if (e.keyCode == "37" && direction != "right") direction = "left";
           else if(e.keyCode == "38" && direction != "down") direction = "up";
           else if(e.keyCode == "39" && direction != "left") direction = "right";
           else if(e.keyCode == "40" && direction != "up") direction = "down";
  }
  
  //funckje kolizji
    
    function collission(x,y,array){
    for(var i = 3; i<array.length; i++){
        if(array[i].x == x && array[i].y == y){
            return true;
            
        }
        return false;
    }
    
    
}

    
//wykrywanie kolizji    
    
    if(nx == -1 || nx == w/size || ny== -1 || ny == h/size || collission(nx,ny,snakeArray))
       
       {
       
       init();
       return;
       
       }  
    
    
//zjedzenie owocu    
     if(nx == food.x && ny == food.y){
        
        var tail = {x:nx,y:ny};
        score++;
        createFood();
     
     }
    
    else{
    
    var tail = snakeArray.pop();
    tail.x = nx;
    tail.y = ny;
    
    }
    
    
    
    
    
    snakeArray.unshift(tail);
    
    
    //funkcja rysowania komorki
    function paintCell(x,y,z){
    ctx.fillStyle=z;
    ctx.fillRect(x*size,y*size,size,size);
    ctx.strokeStyle="white";
    ctx.strokeRect(x*size,y*size,size,size);
    
}

    //implementacja rysowaia weza i jedzenia
    for(var i=0; i<snakeArray.length; i++){
    var c = snakeArray[i]; 
    paintCell(c.x,c.y,"blue");
    
    }
    paintCell(food.x,food.y,"red");
    
    var scoretext = "Wynik: " + score;
    ctx.fillText(scoretext,5,475);
    ctx.font = "25px Arial";
}; 


init()







